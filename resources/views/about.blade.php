@extends('admin.layouts.admin')

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-body text-black-50" style="font-size: 25px; text-decoration: none;">
                    <span>Dastur softix jamoasi tomonidan yaratilgan.</span><br>
                    <span>Dastur haqida savollar bo'lsa yoki qandaydir muammo bo'lsa bizga murojaat qiling.</span><br>
                    <span class="fa fa-globe"></span> <a href="https://softix.uz" target="_blank" class="a">softix.uz</a><br>
                    <span class="fa fa-paper-plane"></span> <a href="https://t.me/Karimov_Ollobergan" target="_blank" class="a">Telegram</a><br>
                    <span class="fa fa-phone"></span> <a href="tel:+998975270606" target="_blank" class="a">+998(97)527-06-06</a><br>
                </div>
            </div>
        </div>
    </div>
    <style>
        .a {
            color: rgba(0,0,0,.5)!important;
            text-decoration: none;
        }

        .a:hover {
            color:rgba(0,0,0,.5)!important;
            text-decoration:none;
            cursor:pointer;
        }
    </style>
@endsection


