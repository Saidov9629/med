@extends('admin.layouts.admin')

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-body">
                    <h1>{{__('admin.Welcome')}} {{\Illuminate\Support\Facades\Auth::user()->user_full_name}}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection


