

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <style>
        th {
            text-align: end;
        }
    </style>
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header"><h4 style="font-family: Georgia">{{__('admin.Client_section')}}</h4></div>
                <div >
                    <button type="button" onclick="sweetconfirm('/admin/client/change_status/{{$order->id}}/endprocess')"  class="btn btn-outline-danger" style="margin-top: 10px; margin-left: 10px;" align="right"><span class="fa fa-remove"></span> {{__('admin.End_diagnosis')}} </button>
                    <a href="{{url('/admin/client/make-order/'.$order->account_id)}}" class="btn btn-outline-success" style="margin-top: 10px; margin-left: 10px;" align="right"><span class="cil cil-loop"></span> {{__('admin.add_queue')}}</a>
                </div>
                <div class="card-body">
                    <form action="{{url('admin/queue/save/'.$order->id)}}" method="POST" id="formdiagnosis">
                        @csrf
                        <div class="pull-left">
                        </div>
                        <table>
                            <tr>
                                <th>{{__('admin.Client_name')}} : </th>
                                <td> <em> {{$account->account_full_name}}</em></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.Phone')}} : </th>
                                <td> <em> {{$account->account_phone}}</em></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.Adress')}} : </th>
                                <td> <em>{{$region->NAME}}, {{$district->DNAME}}, {{$account->account_adress_name}}</em></td>
                            </tr>
                        </table>

                        <hr>
                        <div>
                            <select name="result_template_id" id="result_templates">
                                <option value="0" data-template=""  >{{__('admin.Not selected')}}</option>
                                @foreach($result_templates as $item)
                                    <option value="{{$item->id}}" {{$item->id==$order->result_template_id?"selected":""}} data-template="{{addslashes($item->template)}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div>
                            <fieldset class="border p-2">
                                <legend  class="w-auto" style="font-family: Georgia; color: #4a5568; size: 10px;"> {{__('admin.Write_diagnosis')}} </legend>
                                <div class="form-group">
                                    <textarea class="form-control" id="diagnosis" name="diagnosis" rows="10"  type="text" >{!! $order->diagnosis !!}</textarea>
                                </div>
                            </fieldset>
                            <br>
                            <div class="pull-right">
                                <button type="button" id="save_btn" class="btn btn-behance">{{__('admin.Save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>

    </style>
@endsection
@section('scripts')
    @include('admin.layouts.datatables_js')
    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            tinymce.init({
                force_br_newlines : true,
                force_p_newlines : false,
                forced_root_block : '',
                selector: "textarea",
                content_css: [
                    '{{asset('assets/css/table.css')}}'
                ],
                theme: "modern",
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
                toolbar2: "print preview code ",
                image_advtab: true ,
                external_filemanager_path:"/filemanager/",
                filemanager_title:"Responsive Filemanager" ,
                external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
            });
            $('#result_templates').on('change',function () {
                tinymce.get('diagnosis').setContent($(this).find(':selected').data('template'));
            });

            $('#save_btn').click(function () {
                $('#save_btn').text('Saving...'); //change button text
                $('#save_btn').attr('disabled',true); //set button enable

                var error = 0;
                if ($('#diagnosis').val() === ''){
                    toastr["warning"]('{{__('admin.Please_write_diagnosis')}}','');
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false); //set button enable
                    error = 1;
                    $('#diagnosis').addClass('is-invalid')
                }else{
                    $('#diagnosis').removeClass('is-invalid')
                }


                if (error === 0) {
                    $('#diagnosis').val(tinymce.get('diagnosis').getContent());
                    $.ajax({
                        url : "{{route('admin.queue.save', $order->id)}}",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: $('#formdiagnosis').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {

                            if(data.success)
                            {
                                toastr["success"](data.message,'');
                                $('#serviceModal').modal('hide');
                            }
                            else
                            {
                                Swal.fire({
                                    icon: 'warning',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                            table.ajax.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: "{{__('admin.Error_server')}}",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                            table.ajax.reload();
                        }
                    });
                }
            });
        } );



    </script>
@endsection
