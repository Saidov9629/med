

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Report by day')}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="{{url('/admin/report/by-day')}}" method="get">
                                <label for="">Hisobot kuni</label>
                                <div class="input-group">
                                    <input class="date-input form-control" name="day" value="{{$day}}">
                                    <button class="btn btn-outline-primary" type="submit">{{__('admin.Show')}}</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td class="left"><strong>{{__('admin.Total')}}</strong></td>
                                    <td class="right">{{$total_data!=null?number_format($total_data->total_payed):'0'}}</td>
                                </tr>
                                <tr>
                                    <td class="left"><strong>{{__('admin.Cash')}}</strong></td>
                                    <td class="right">{{$total_data!=null?number_format($total_data->total_cash):'0'}}</td>
                                </tr>
                                <tr>
                                    <td class="left"><strong>{{__('admin.Card')}}</strong></td>
                                    <td class="right">{{$total_data!=null?number_format($total_data->total_card):'0'}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <h2>Batafsil</h2>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.Client_name')}} </th>
                            <th> {{__('admin.Phone')}} </th>
                            <th> {{__('admin.Total')}} </th>
                            <th> {{__('admin.Cash')}} </th>
                            <th> {{__('admin.Card')}} </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $item)
                            <tr>
                                <td></td>
                                <td>{{$item->transaction_account->account_full_name}}</td>
                                <td>{{$item->transaction_account->account_phone}}</td>
                                <td>{{number_format($item->total_payed)}}</td>
                                <td>{{number_format($item->total_cash)}}</td>
                                <td>{{number_format($item->total_card)}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('assets/js/daterangepicker.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterangepicker.css')}}" />
    <script type="text/javascript">

        $(document).ready(function(){
            $('.date-input').daterangepicker({
                singleDatePicker: true,
                showDropdowns: false,
                timePicker: false,
                timePicker24Hour:true,
                minYear: 2000,
                locale: {
                    format: 'YYYY-MM-DD'
                },
                maxYear: parseInt(moment().format('YYYY-MM-DD'))
            });
        });

    </script>
@endsection
