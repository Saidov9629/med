@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Inprocess_clents')}}</div>
                <div class="card-body">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>{{__('admin.Name')}}</th>
                            <th>{{__('admin.Servese_type')}}</th>
                            <th>{{__('admin.service_fee_status')}}</th>
                            <th>{{__('admin.Write_diagnosis')}}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')

    <script type="text/javascript">

        var table;
        $(document).ready(function() {

            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: [[1, 'asc']],
                ajax: {
                    url: "{{ route('admin.inprocess.datatable') }}",
                    type: "POST",
                    data: {
                        "_token": "{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'order_account.account_full_name'},
                    {data: 'order_service.name'},
                    {data: 'status'},
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    </script>
@endsection

