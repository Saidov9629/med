@extends('admin.layouts.admin')

@section('content')

    <style>
        th {
            text-align: end;
        }
        h5 {
            font-family: Calibri;
            color: darkblue;
            size: 8px;
        }
    </style>

    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header"><h3 style="color: #0b2e13; size: 26px; font-family: 'Times New Roman';">
                        <i  style="color: deeppink;" class="cil cil-keyboard"></i> {{__('admin.Change_password')}}</h3></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-4" style="background-color: #e0e0e0; border-radius: 10px;">
                            <form action="{{ route('admin.checkupdatepas') }}" method="POST" id="account_form">
                                @csrf
                                <div class="form-group">
                                    <label for="user_full_name">{{__('admin.User_full_name')}} : </label>
                                    <input class="form-control" id="account_full_name" name="user_full_name" value="{{$user->user_full_name}}" type="text" >
                                </div>
                                <div class="form-group">
                                    <label for="user_full_name">{{__('admin.Login')}} : </label>
                                    <input class="form-control" id="account_full_name" name="user_login" value="{{$user->user_login}}" type="text" >
                                </div>
                                <div class="form-group">
                                    <label for="user_full_name">{{__('admin.Old_password')}} :</label>
                                    <input class="form-control" id="old_pas" name="old_password" type="password" >
                                </div>
                                <div class="form-group">
                                    <label for="user_full_name">{{__('admin.New_password')}} :</label>
                                    <input class="form-control" id="pas" name="new_password" type="password" required>
                                </div>
                                <div class="form-group">
                                    <label for="user_full_name">{{__('admin.Conforim_password')}} :</label>
                                    <input class="form-control" id="pas" name="confirm_password" type="password" required>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-success" type="send" id="save_btn">{{__('admin.Save')}}</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">

                        </div>
                     <div>
                </div>
            </div>
        </div>
    </div>
@endsection
