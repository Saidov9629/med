

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Most_pay_for_services')}}</div>
                <div class="card-body">
                    <div class="pull-left">
                    </div>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.Client_name')}} </th>
                            <th> {{__('admin.Summa')}} </th>
                            <th> {{__('admin.Action')}} </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')
    <script type="text/javascript">
        var table;
        var edit_add;
        $(document).ready(function() {
            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: false,
                ajax: {
                    url: "{{ route('admin.not_paid.datatable') }}",
                    type: "POST",
                    data:{
                        "_token":"{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'order_account.account_full_name'},
                    {data: 'total'},
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    </script>
@endsection
