

@extends('admin.layouts.admin')


@section('content')
    <style>
        th {
            text-align: end;
        }

    </style>

    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Client_info')}}</div>
                <div class="card-body">
                    <form action="{{url('admin/not-paid/save')}}" method="POST">
                        @csrf
                        <input type="hidden" name="account_id" value="{{$account->account_id}}">
                        <div class="pull-left">
                        </div>
                        <table>
                            <tr>
                                <th>{{__('admin.Client_name')}} : </th>
                                <td> <em> {{$account->account_full_name}}</em></td>
                            </tr>
                            <tr>
                                <th > {{__('admin.Phone')}} : </th>
                                <td> <em> {{$account->account_phone}}</em></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.Adress')}} : </th>
                                <td> <em>{{$region->NAME}}, {{$district->DNAME}}, {{$account->account_adress_name}}</em></td>
                            </tr>
                        </table>

                        <hr>
                        <div>
                            <fieldset class="border p-2">
                                <legend  class="w-auto" style="font-family: Georgia; color: #4a5568; size: 10px;"> {{__('admin.Servese_type')}} </legend>
                                <?php $total = 0;?>
                                <table  class="table table-bordered" id="dataTable">
                                    <thead>
                                    <tr >
                                        <th>№</th>
                                        <th > {{__('admin.Servese_name')}}</th>
                                        <th> {{__('admin.Price')}}  </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td><em>{{$order->account_id}}</em></td>
                                            <td><em>{{$order->order_service->name}}</em></td>
                                            <td><em>{{number_format($order->price,0)}} sum</em></td>
                                        </tr>
                                        <?php $total += $order->price;?>

                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-2">
                                        <h2>Jami: {{number_format($total,0)}}</h2>
                                        <div class="form-group">
                                            <label for="company">{{__('admin.Cash')}}</label>
                                            <input class="form-control" id="naxt" name="cash" type="number" >
                                        </div>
                                        <div class="form-group">
                                            <label for="company">{{__('admin.Card')}}</label>
                                            <input class="form-control" id="plastik" name="card" type="number" >
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </fieldset>
                            <br>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-behance">{{__('admin.Save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

