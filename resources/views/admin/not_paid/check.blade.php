

<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        * {
            font-size: 12px;
            font-family: 'Calibri';
        }


        thead,
        table {
            border-collapse: collapse;
        }

        tbody{
            border-bottom: 1px solid black;
            border-collapse: collapse;
        }

        td.description,
        th.description {
            width: 30%;
            max-width: 30%;
        }

        td.quantity,
        th.quantity {
            width: 40px;
            max-width: 40px;
            word-break: break-all;
        }
        .line{
            border-bottom: 1px solid black;
        }
        td.price,
        th.price {
            width: 85px;
            max-width: 85px;
            text-align: right;
            word-break: break-all;
        }

        .centered {
            text-align: center;
            align-content: center;
        }

        .ticket {
            width: 95%;
            max-width: 95%;

        }

        img {
            max-width: inherit;
            width: inherit;
        }

        @media print {
            .hidden-print,
            .hidden-print * {
                display: none !important;
            }
        }
    </style>
    <title>{{__('admin.Conpaniya_name')}}</title>
</head>
<body >
<div class="ticket">
    <p align="center" > <b>"{{__('admin.Conpaniya_name')}}"</b> <br>
       {{__('admin.clinica')}}</p>
    <table>
        @foreach($data as $item)
            <tr>
                <td class="description" colspan="2" style="text-align: left">{{ $item->order_service->name }}</td>
            </tr>
            <tr class="line">
                <td class="price">{{ number_format($item->price, 0, '.', ',')}} sum</td>
            </tr>
        @endforeach
    </table>

    <div style="margin: 10px 0 10px 0;">
            <article class="price" style="text-align: right">
                Jami: <strong style="font-size: 18px !important;">{{ number_format( $transaction->credit, 0, '.', ','). " " }} sum</strong>
            </article>

    </div>
    <p align="center">{{ $transaction->created_at }}</p>

    <footer class="centered">{{__('admin.thanks_for_visiting')}}</footer>
    <br>
    <hr>
</div>
<script>
    window.onload=function () {
        window.print();
    }
</script>
</body>
</html>
