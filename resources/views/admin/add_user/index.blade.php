

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Add_user')}}</div>
                <div class="card-body">
                    <div class="pull-left">
                        <button class="btn btn-outline-primary" id="addBtn"><span class="fa fa-plus"></span> {{__('admin.Add_user')}}</button>
                    </div>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.User_full_name')}} </th>
                            <th> {{__('admin.User_login')}}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form action="#" id="account_form">
                        @csrf
                        <input type="hidden" name="user_id" id="account_id" value="0">
                        <div class="form-group">
                            <label for="company"> {{__('admin.User_full_name')}} :</label>
                            <input class="form-control" id="user_full_name" name="user_full_name"  type="text" >
                        </div>
                        <div class="form-group">
                            <label for="company">{{__('admin.User_login')}} : </label>
                            <input class="form-control" id="user_login" name="user_login" type="text" >
                        </div>

                        <div class="form-group">
                            <label for="company">{{__('admin.Parol')}} :</label>
                            <input class="form-control" id="user_password" name="user_password"  type="password" >
                        </div>
                        <div class="form-group">
                            <label for="company">{{__('admin.Conforim_password')}} :</label>
                            <input class="form-control" id="user_conforim_password" name="user_conforim_password" type="password" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn">{{__('admin.Save')}}</button>
                </div>
            </div>

        </div>

    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')
    <script type="text/javascript">


        var table;
        var edit_add;
        $(document).ready(function() {

            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 1, 'asc' ]],
                ajax: {
                    url: "{{ route('admin.add_user.datatable') }}",
                    type: "POST",
                    data:{
                        "_token":"{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'user_full_name'},
                    {data: 'user_login'},
                ]
            });



            $('#addBtn').click(function () {
                $('#modal_title').html("{{__('admin.Add_user')}}");
                $('#user_id').val(0);
                $('#user_full_name').val('');
                $('#user_login').val('');
                $('#accountModal').modal('show');
                $('#user_full_name').removeClass('is-invalid')
                $('#user_login').removeClass('is-invalid')

            })
            $('#save_btn').click(function () {
                $('#save_btn').text('Saving...'); //change button text
                $('#save_btn').attr('disabled',true); //set button enable

                var error = 0;
                if ($('#user_full_name').val() === ''){
                    toastr["warning"]('{{__('admin.Enter_user_full_name')}}','');
                    error = 1;
                    $('#user_full_name').addClass('is-invalid')
                }else{
                    $('#user_full_name').removeClass('is-invalid')
                }

                if ($('#user_login').val() === '' ){
                    toastr["warning"]('{{__('admin.Enter_login')}}','');
                    error = 1;
                    $('#user_login').addClass('is-invalid')
                }else{
                    $('#user_login').removeClass('is-invalid')
                }

                if ($('#user_password').val() === ''){
                    toastr["warning"]('{{__('admin.User_password')}}','');
                    error = 1;
                    $('#user_password').addClass('is-invalid')
                }else{
                    $('#user_password').removeClass('is-invalid')
                }

                if ($('#user_conforim_password').val() === ''){
                    toastr["warning"]('{{__('admin.Conforim_new_password')}}','');
                    error = 1;
                    $('#user_conforim_password').addClass('is-invalid')
                }else{
                    $('#user_conforim_password').removeClass('is-invalid')
                }



                if (error === 0) {
                    $.ajax({
                        url : "{{route('admin.add_user.save')}}",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: $('#account_form').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {
                            if(data.success)
                            {
                                toastr["success"](data.message,'');
                                $('#accountModal').modal('hide');
                                if (edit_add == 1){
                                    window.location="{{url('/admin/add_user/index')}}/"+data.user_id;
                                }else{
                                    table.ajax.reload();
                                }
                            }
                            else
                            {
                                Swal.fire({
                                    // position: 'top-end',
                                    icon: 'warning',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })

                            }


                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: "{{__('admin.Error_server')}}",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        }
                    });
                }else{
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false);
                }


            });
        } );


        function edit(id,user_full_name,user_login) {
            $('#modal_title').html("{{__('admin.Edit_user_info')}}");
            $('#user_id').val(id);
            $('#user_full_name').val(user_full_name);
            $('#user_login').val(user_login);

            $('#accountModal').modal('show');
            $('#user_full_name').removeClass('is-invalid')
            $('#user_login').removeClass('is-invalid')
        }
    </script>
@endsection
