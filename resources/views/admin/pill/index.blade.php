

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Pills')}}</div>
                <div class="card-body">
                    <div class="pull-left">
                        @if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                            <button class="btn btn-outline-primary" id="addBtn"><span class="fa fa-plus"></span> {{__('admin.Add_pill')}}</button>
                        @endif
                    </div>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.Pill_name')}} </th>
                            <th> {{__('admin.Pill_price')}} </th>
                            <th> {{__('admin.Quantity')}} </th>
                            <th> {{__('admin.Action')}} </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" >
                    <form action="#" id="account_form">
                        @csrf
                        <input type="hidden" name="pill_id" id="pill_id" value="0">
                        <div class="form-group">
                            <label for="pill_name">{{__('admin.Pill_name')}}</label>
                            <input class="form-control" id="pill_name" name="pill_name" type="text" >
                        </div>
                        <div class="form-group">
                            <label for="pill_price">{{__('admin.Pill_price')}}</label>
                            <input class="form-control" id="pill_price" name="pill_price" type="number" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn">{{__('admin.Save')}}</button>
                </div>
            </div>

        </div>

    </div>
    <div class="modal fade" id="cargoModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title_cargo"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form action="#" id="cargo_form">
                        @csrf
                        <input type="hidden" name="pill_id" id="cargo_pill_id" value="0">
                        <div class="form-group">
                            <label for="quantity">{{__('admin.Quantity')}}</label>
                            <input class="form-control" id="quantity" name="quantity" type="number" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn_cargo">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')
    <script type="text/javascript">

        var table;
        var edit_add;
        $(document).ready(function() {
            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('admin.pill.datatable') }}",
                    type: "POST",
                    data:{
                        "_token":"{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'pill_name'},
                    {data: 'pill_price'},
                    {data: 'pill_quantity'},
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });



            $('#addBtn').click(function () {
                $('#modal_title').html("{{__('admin.Add_pill')}}");
                $('#account_id').val(0);
                edit_add = 1;
                $('#pill_name').val('');
                $('#pill_price').val('');
                $('#accountModal').modal('show');
                $('#pill_name').removeClass('is-invalid');
                $('#pill_price').removeClass('is-invalid');

            })
            $('#save_btn').click(function () {
                $('#save_btn').text('Saving...'); //change button text
                $('#save_btn').attr('disabled',true); //set button enable

                var error = 0;
                if ($('#pill_name').val() === ''){
                    toastr["warning"]('{{__('admin.Insert_pill_name')}}','');
                    error = 1;
                    $('#pill_name').addClass('is-invalid')
                }else{
                    $('#pill_name').removeClass('is-invalid')
                }
                if ($('#pill_price').val() === '' ){
                    toastr["warning"]('{{__('admin.Insert_pill_price')}}','');
                    error = 1;
                    $('#pill_price').addClass('is-invalid')
                }else{
                    $('#pill_price').removeClass('is-invalid')
                }



                if (error === 0) {
                    $.ajax({
                        url : "{{route('admin.pill.save')}}",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: $('#account_form').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {

                            if(data.success)
                            {
                                toastr["success"](data.message,'');
                                $('#accountModal').modal('hide');
                                table.ajax.reload();
                            }
                            else
                            {
                                Swal.fire({
                                    // position: 'top-end',
                                    icon: 'warning',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })

                            }
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: "{{__('admin.Error server')}}",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        }
                    });
                }else{
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false);
                }


            });

            $('#save_btn_cargo').click(function () {
                $.ajax({
                    url : "{{route('admin.pill.cargo_save')}}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    data: $('#cargo_form').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {

                        if(data.success)
                        {
                            toastr["success"](data.message,'');
                            $('#cargoModal').modal('hide');
                            table.ajax.reload();
                        }
                        else
                        {
                            Swal.fire({
                                // position: 'top-end',
                                icon: 'warning',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })

                        }
                        $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                        $('#save_btn').attr('disabled',false); //set button enable
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        Swal.fire({
                            icon: 'error',
                            title: "{{__('admin.Error server')}}",
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                        $('#save_btn').attr('disabled',false); //set button enable
                    }
                });
            });
        } );


        function edit(id,pill_name,pill_price) {
            edit_add = 2;
            $('#modal_title').html("{{__('admin.Edit_pill')}}");
            $('#pill_id').val(id);
            $('#pill_name').val(pill_name);
            $('#pill_price').val(pill_price);
            $('#accountModal').modal('show');
            $('#pill_name').removeClass('is-invalid');
            $('#pill_price').removeClass('is-invalid');
        }

        function add_cargo(id,pill_name) {
            $('#modal_title_cargo').html("{{__('admin.Add cargo')}}: "+pill_name);
            $('#cargo_pill_id').val(id);
            $('#cargo_form').trigger('reset');
            $('#cargoModal').modal('show');
        }
    </script>
@endsection
