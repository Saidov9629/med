

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <style>
        th {
            text-align: end;
        }

    </style>
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header"><h4 style="font-family: Georgia">{{__('admin.Client pills')}}</h4></div>
                <div class="card-body">
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" onclick="add_pill()"><span class="fa fa-medkit"></span> Dori qo'shish</button>
                    </div>
                    <table>
                        <tr>
                            <th>{{__('admin.Client_name')}} : </th>
                            <td> <em> {{$account->account_full_name}}</em></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.Phone')}} : </th>
                            <td> <em> {{$account->account_phone}}</em></td>
                        </tr>
                        <tr>
                            <th> {{__('admin.Adress')}} : </th>
                            <td> <em>{{$region->NAME}}, {{$district->DNAME}}, {{$account->account_adress_name}}</em></td>
                        </tr>
                    </table>
                    <hr>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.Client_name')}} </th>
                            <th> {{__('admin.Pill_name')}} </th>
                            <th> {{__('admin.Quantity')}} </th>
                            @if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                                <th> {{__('admin.Pill_price')}} </th>
                            @endif
                            <th> {{__('admin.Employee')}} </th>
                            <th> {{__('admin.Date')}} </th>
                            @if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                                <th> {{__('admin.Action')}} </th>
                            @endif
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" style="background-color: #e0e0e0;">
                    <form action="#" id="account_form">
                        @csrf
                        <input type="hidden" name="account_id" id="account_id" value="{{$account->account_id}}">
                        <div class="form-group">
                            <label for="pill_name">{{__('admin.Pill_name')}}</label>
                            <select name="pill_id" id="pill_id" style="width: 100%">
                                <option value="">--Iltimos tanlang--</option>
                                @foreach($pills as $item)
                                    <option value="{{$item->pill_id}}">{{$item->pill_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="quantity">{{__('admin.Quantity')}}</label>
                            <input class="form-control" id="quantity" name="quantity" type="number" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    @include('admin.layouts.datatables_js')

    <script>
        var table;
        window.onload = function () {
            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 0, 'desc' ]],
                ajax: {
                    url: "{{ route('admin.pill.clientpills',$account->account_id) }}",
                    type: "POST",
                    data:{
                        "_token":"{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'pill_order_id', searchable: false},
                    {data: 'order_account.account_full_name',orderable: false, },
                    {data: 'order_pill.pill_name',orderable: false, },
                    {data: 'quantity'},
                        @if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                    {data: 'pill_price'},
                        @endif
                    {data: 'pill_order_user.user_full_name'},
                    {data: 'created_at'},
                        @if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                    {data: 'action'},
                    @endif

                ]
            });


            $('#pill_id').select2();
            $('#save_btn').click(function () {
                $.ajax({
                    url : "{{route('admin.pill.client_pill_save')}}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    data: $('#account_form').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {

                        if(data.success)
                        {
                            toastr["success"](data.message,'');
                            $('#accountModal').modal('hide');
                            table.ajax.reload();
                        }
                        else
                        {
                            Swal.fire({
                                // position: 'top-end',
                                icon: 'warning',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })

                        }
                        $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                        $('#save_btn').attr('disabled',false); //set button enable
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        Swal.fire({
                            icon: 'error',
                            title: "{{__('admin.Error server')}}",
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                        $('#save_btn').attr('disabled',false); //set button enable
                    }
                });
            });
        }
        function add_pill() {
            $('#account_form').trigger("reset");
            $('#accountModal').modal('show');
        }
    </script>
@endsection
