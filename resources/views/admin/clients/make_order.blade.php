

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <style>
        th {
            text-align: end;
        }

    </style>
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header"><h4 style="font-family: Georgia">{{__('admin.Add_service_for_client')}}</h4></div>
                <div class="card-body">
                    <form action="{{url('admin/client/make-order/'.$account->account_id)}}" method="POST">
                        @csrf
                        <input type="hidden" name="account_id" value="{{$account->account_id}}">
                        <div class="pull-left">
                        </div>
                        <table>
                            <tr>
                                <th>{{__('admin.Client_name')}} : </th>
                                <td> <em> {{$account->account_full_name}}</em></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.Phone')}} : </th>
                                <td> <em> {{$account->account_phone}}</em></td>
                            </tr>
                            <tr>
                                <th> {{__('admin.Adress')}} : </th>
                                <td> <em>{{$region->NAME}}, {{$district->DNAME}}, {{$account->account_adress_name}}</em></td>
                            </tr>
                        </table>

                        <hr>
                        <div>
                            @foreach($categories as $category)
                                @if(count($category->category_services)>0)
                                    <fieldset class="border p-2">
                                        <legend  class="w-auto" style="font-family: Georgia; color: #4a5568; size: 10px;"> {{$category->category_name}} </legend>
                                            <div class="row">
                                                @foreach($category->category_services as $service)
                                                    <div class="col-md-3">
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input service" onchange="calculate_total()" name="service[]" id="{{$service->id}}" value="{{$service->id}}" data-price="{{$service->price}}">
                                                            <label class="form-check-label" style="color: navy" for="{{$service->id}}">{{$service->name}} <b>( {{number_format($service->price)}} )</b> </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                    </fieldset>
                                @endif
                            @endforeach

                            <br>
                            <div class="pull-right">
                                <h5>{{__('admin.Total_price')}} <span id="total_sum">0</span> {{__('admin.money_type')}}</h5>
                                <button type="submit" class="btn btn-behance">Saqlash</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

    function calculate_total() {
        var total = 0;
        $("input:checked.service").each(function () {
            total+=parseInt($(this).attr('data-price'));
        });
        $('#total_sum').text(numeral(total).format('0,0'));
    }
</script>
