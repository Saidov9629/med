@extends('admin.layouts.admin')

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Client_info')}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="row">
                                <dt class="col-sm-3">{{__('admin.Client_name')}}</dt>
                                <dd class="col-sm-9">{{$account->account_full_name}}</dd>
                                <dt class="col-sm-3">{{__('admin.Client_phone')}}</dt>
                                <dd class="col-sm-9">{{$account->account_phone}}</dd>
                                <dt class="col-sm-3">{{__('admin.Region')}}</dt>
                                <dd class="col-sm-9">{{$account->account_region->NAME}}</dd>
                                <dt class="col-sm-3">{{__('admin.District')}}</dt>
                                <dd class="col-sm-9">{{$account_district->DNAME}}</dd>
                                <dt class="col-sm-3">{{__('admin.Loan')}}</dt>
                                <dd class="col-sm-9">{!! $account_loan !!}</dd>
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary" type="button" onclick="open_loan_modal()" ><span class="fa fa-plus-circle"></span> {{__('admin.Loan return')}}</button>
                            <button class="btn btn-primary" type="button" onclick="open_discount_modal()" ><span class="fa fa-plus-circle"></span> Chegirma</button>
                            <hr>
                            <button class="btn btn-primary" disabled type="button"><span class="fa fa-clipboard"></span> {{__('admin.Orders')}}</button>
                            <a href="{{url('/admin/client/view/'.$account->account_id)}}" class="btn btn-primary" type="button"><span class="fa fa-money"></span> {{__('admin.Transactions')}}</a>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>Summa</td>
                            <td>{{__('admin.Payment type')}}</td>
                            <td>Status</td>
                            <td>{{__('admin.Date')}}</td>
                            <td>{{__('admin.Action')}}</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($services as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->order_service->name}}</td>
                                <td>{{number_format($item->price)}}</td>
                                <td><?php
                                    switch ($item->status){
                                        case 'inprocess': echo "<span class='badge badge-primary'>".__('admin.inprocess')."</span>"; break;
                                        case 'endprocess': echo "<span class='badge badge-success'>".__('admin.endprocess')."</span>"; break;
                                        case 'cancel': echo "<span class='badge badge-danger'>".__('admin.cancel')."</span>"; break;
                                    }
                                    ?></td>
                                <td>{{date('Y-m-d H:i',strtotime($item->created_at))}}</td>
                                <td>
                                    <a href="{{url('/admin/order/print/'.$item->id)}}" target="_blank" class="btn btn-primary"><span class="fa fa-eye"></span></a>
                                    @if((Illuminate\Support\Facades\Auth::user()->user_role_id == 1 && $item->status!='cancel') ||  $item->status == 'inprocess')
                                        <button type="button" class="btn btn-danger" onclick="cancel_order({{$item->id}})"><span class="fa fa-ban"></span></button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$services->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title">{{__('admin.Loan return')}}</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" >
                    <form action="#" id="account_form">
                        @csrf
                        <div class="form-group">
                            <label for="payment_type">{{__('admin.Payment type')}}</label>
                            <select name="payment_type" id="payment_type" class="form-control">
                                <option value="cash">{{__('admin.Cash')}}</option>
                                <option value="card">{{__('admin.Card')}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="summa">Summa</label>
                            <input class="form-control" id="summa" name="summa" type="number" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" onclick="save()">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="orderModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title">{{__('admin.Order cancel')}}</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" >
                    <form action="#" id="order_form">
                        @csrf
                        <input type="hidden" name="order_id" id="order_id">
                        <div class="form-group">
                            <label for="summa">{{__('admin.Returned summa')}}</label>
                            <input class="form-control" id="summa" name="summa" type="number" value="0">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" onclick="save_cancel_order()">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal2" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title">Chegirma </h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" >
                    <form action="#" id="account_form2">
                        @csrf
                        <input name="payment_type" value="discount" type="hidden">
                        <div class="form-group">
                            <label for="summa">Summa</label>
                            <input class="form-control" id="summa" name="summa" type="number" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" onclick="save2()">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        function open_loan_modal() {
            $('#account_form').trigger('reset');
            $('#accountModal').modal('show');
        }
        function open_discount_modal() {
            $('#account_form2').trigger('reset');
            $('#accountModal2').modal('show');
        }
        function save(){
            $.ajax({
                url : "{{url('admin/client/save-transaction/'.$account->account_id)}}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: $('#account_form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.success)
                    {
                        toastr["success"](data.message,'');
                        $('#accountModal').modal('hide');
                        location.reload();
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'warning',
                            title: data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Swal.fire({
                        icon: 'error',
                        title: "{{__('admin.Error server')}}",
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                }
            });
        }

        function cancel_order(id) {
            $('#order_id').val(id);
            $('#order_form').trigger('reset');
            $('#orderModal').modal('show');
        }
        function save_cancel_order(){
            $.ajax({
                url : "{{url('admin/order/cancel/'.$account->account_id)}}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: $('#order_form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.success)
                    {
                        toastr["success"](data.message,'');
                        $('#accountModal').modal('hide');
                        window.location.href = '{{url('/admin/client/view/'.$account->account_id)}}';
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'warning',
                            title: data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Swal.fire({
                        icon: 'error',
                        title: "{{__('admin.Error server')}}",
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                }
            });
        }

        function save2(){
            $.ajax({
                url : "{{url('admin/client/save-transaction/'.$account->account_id)}}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: $('#account_form2').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    if(data.success)
                    {
                        toastr["success"](data.message,'');
                        $('#accountModal').modal('hide');
                        location.reload();
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'warning',
                            title: data.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    Swal.fire({
                        icon: 'error',
                        title: "{{__('admin.Error server')}}",
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $('#save_btn').text('{{__('admin.Save')}}');
                    $('#save_btn').attr('disabled',false);
                }
            });
        }

    </script>
@endsection
