

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Client_info')}}</div>
                <div class="card-body">

                    <div class="pull-left">
                        @if(in_array(3,\Illuminate\Support\Facades\Auth::user()->user_permissions))
                            <button class="btn btn-outline-primary" id="addBtn"><span class="fa fa-plus"></span> {{__('admin.Add_client')}}</button>
                        @endif
                    </div>
                    <div class="pull-left">
                        <select name="show_type" id="show_type" class="form-control input-small ml-1" onchange="refresh_table()">
                            <option value="today">Bugungi</option>
                            <option value="all">Barchasi</option>
                        </select>
                    </div>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th> {{__('admin.Client_name')}} </th>
                            <th> {{__('admin.Phone')}} </th>
                            <th> {{__('admin.Loan')}} </th>
                            <th> {{__('admin.Action')}} </th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="accountModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" style="background-color: #e0e0e0;">
                    <form action="#" id="account_form">
                        @csrf
                        <input type="hidden" name="account_id" id="account_id" value="0">
                        <div class="form-group">
                            <label for="company">{{__('admin.Client_name')}}</label>
                            <input class="form-control" id="account_full_name" name="account_full_name" type="text" >
                        </div>
                        <div class="form-group">
                            <label for="company">{{__('admin.Phone')}}</label>
                            <input class="form-control" id="account_phone" name="account_phone" type="text" >
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="company">{{__('admin.Region')}} </label>
                                <select class="form-select form-control region" id="account_region_code" name="account_region_code" >
                                    <option value="0" disabled="true" selected="true">-{{__('admin.Select')}}-</option>
                                    @foreach($regions as $item)
                                        <option value="{{$item->CODE}}">{{$item->NAME}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="company">{{__('admin.District')}} </label>
                                <select class="form-select form-control districtname" id="account_district_code" name="account_district_code" >

                                    <option value="0" disabled="true" selected="true">{{__('admin.District_name')}}</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="company">{{__('admin.Adress')}}</label>
                            <input class="form-control" id="account_adress_name" name="account_adress_name" type="text" >
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn">{{__('admin.Save')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')
    <script type="text/javascript">

        $(document).ready(function(){

            $('.region').on('change',function(){

                var region_id=$(this).val();

                var op=" ";

                $.ajax({
                    type:'get',
                    url:'{!!url('admin/client/findDistrictName')!!}',
                    data:{'CODE':region_id},
                    success:function(data){

                        op+='<option value="0" selected disabled>{{__('admin.Select_district_name')}}</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].CODE+'">'+data[i].DNAME+'</option>';
                        }


                        $('#account_district_code').html(op);
                    },
                    error:function(){

                    }
                });
            });
        });

        var table;
        var edit_add;
        $(document).ready(function() {
            $.mask.definitions['9'] = '';
            $.mask.definitions['d'] = '[0-9]';
            $('#account_phone').mask("+(998) dd ddd-dd-dd");

            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 1, 'asc' ]],
                ajax: {
                    url: "{{ route('admin.clients.datatable') }}",
                    type: "POST",
                    data: function (d) {
                        d._token = "{{csrf_token()}}";
                        d.show_type = $('#show_type').val();
                    }

                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'account_full_name'},
                    {data: 'account_phone'},
                    {data: 'account_loan'},
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });



            $('#addBtn').click(function () {
                $('#modal_title').html("{{__('admin.Add_client')}}");
                $('#account_id').val(0);
                edit_add = 1;
                $('#account_full_name').val('');
                $('#account_phone').val('');
                $('#account_region_code').val('33');
                $('#account_region_code').trigger("change");
                $('#account_district_code').val('');
                $('#account_adress_name').val('');
                $('#accountModal').modal('show');
                $('#account_full_name').removeClass('is-invalid')
                $('#account_phone').removeClass('is-invalid')
                $('#account_region_code').removeClass('is-invalid')
                $('#account_district_code').removeClass('is-invalid')
                $('#account_adress_name').removeClass('is-invalid')
            })
            $('#save_btn').click(function () {
                $('#save_btn').text('Saving...'); //change button text
                $('#save_btn').attr('disabled',true); //set button enable

                var error = 0;
                if ($('#account_full_name').val() === ''){
                    toastr["warning"]('{{__('admin.Insert_client_name')}}','');
                    error = 1;
                    $('#account_full_name').addClass('is-invalid')
                }else{
                    $('#account_full_name').removeClass('is-invalid')
                }
                if ($('#account_phone').val() === '' ){
                    toastr["warning"]('{{__('admin.Insert_client_phone')}}','');
                    error = 1;
                    $('#account_phone').addClass('is-invalid')
                }else{
                    $('#account_phone').removeClass('is-invalid')
                }
                if ($('#account_region_code').val() === ''){
                    toastr["warning"]('{{__('admin.Select_region')}}','');
                    error = 1;
                    $('#account_region_codee').addClass('is-invalid')
                }else{
                    $('#account_region_code').removeClass('is-invalid')
                }
                if ($('#account_district_code').val() === ''){
                    toastr["warning"]('{{__('admin.Select_district')}}','');
                    error = 1;
                    $('#account_district_code').addClass('is-invalid')
                }else{
                    $('#account_district_code').removeClass('is-invalid')
                }
                if ($('#account_adress_name').val() === ''){
                    toastr["warning"]('{{__('admin.Enter_adress')}}','');
                    error = 1;
                    $('#account_adress_name').addClass('is-invalid')
                }else{
                    $('#account_adress_name').removeClass('is-invalid')
                }


                if (error === 0) {
                    $.ajax({
                        url : "{{route('admin.client.save')}}",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: $('#account_form').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {

                            if(data.success)
                            {
                                toastr["success"](data.message,'');
                                $('#accountModal').modal('hide');
                                if (edit_add == 1){
                                    window.location="{{url('/admin/client/make-order')}}/"+data.account_id;
                                }else{
                                    table.ajax.reload();
                                }
                            }
                            else
                            {
                                Swal.fire({
                                    // position: 'top-end',
                                    icon: 'warning',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })

                            }
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            Swal.fire({
                                icon: 'error',
                                title: "{{__('admin.Error server')}}",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                        }
                    });
                }else{
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false);
                }


            });
        } );
        function refresh_table() {
            table.ajax.reload();
        }

        function edit(id,full_name,phone,region_name,district_name,adress_name) {
            edit_add = 2;
            var region_id=region_name;
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!url('admin/client/findDistrictName')!!}',
                data:{'CODE':region_id},
                success:function(data){

                    op+='<option value="0" selected disabled>{{__('admin.Select_district_name')}}</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].CODE+'">'+data[i].DNAME+'</option>';
                    }

                    $('#account_district_code').html(op);
                    $('#account_district_code').val(district_name);
                },
                error:function(){

                }
            });

            $('#modal_title').html("{{__('admin.Edit_client')}}");
            $('#account_id').val(id);
            $('#account_full_name').val(full_name);
            $('#account_phone').val(phone);
            $('#account_region_code').val(region_name);

            $('#account_adress_name').val(adress_name);
            $('#accountModal').modal('show');
            $('#account_full_name').removeClass('is-invalid')
            $('#account_phone').removeClass('is-invalid')
            $('#account_region_code').removeClass('is-invalid')
            $('#account_district_code').removeClass('is-invalid')
            $('#account_adress_name').removeClass('is-invalid')
        }
    </script>
@endsection
