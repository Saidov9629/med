@if(in_array(1,\Illuminate\Support\Facades\Auth::user()->user_permissions))
<li class="nav-item {{ Request::is('admin.dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.dashboard') }}">
        <i class="nav-icon fa fa-dashboard"></i>
        <span>{{__('admin.Dashboard')}}</span>
    </a>
</li>
@endif
@if(in_array(2,\Illuminate\Support\Facades\Auth::user()->user_permissions))
<li class="nav-item {{ Request::is('admin.clients*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.clients.index') }}">
        <i class="nav-icon fa fa-users"></i>
        <span> {{__('admin.Clients')}}</span>
    </a>
</li>
@endif
@if(in_array(7,\Illuminate\Support\Facades\Auth::user()->user_permissions))
<li class="nav-item {{ Request::is('admin.pill*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.pill.index') }}">
        <i class="nav-icon fa fa-users"></i>
        <span> {{__('admin.Pills')}}</span>
    </a>
</li>
@endif
@if(in_array(4,\Illuminate\Support\Facades\Auth::user()->user_permissions) || in_array(5,\Illuminate\Support\Facades\Auth::user()->user_permissions) || in_array(10,\Illuminate\Support\Facades\Auth::user()->user_permissions))
<li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.queue.index') }}">
        <i class="nav-icon fa fa-file"></i>
        <span> {{__('admin.queue')}}</span>
    </a>
</li>
@endif

{{--<li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">--}}
{{--    <a class="nav-link" href="{{ route('admin.inprocess.index') }}">--}}
{{--        <i class="nav-icon fa fa-eye"></i>--}}
{{--        <span> {{__('admin.inprocess')}}</span>--}}
{{--    </a>--}}
{{--</li>--}}

{{--<li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">--}}
{{--    <a class="nav-link" href="{{ route('admin.cancel_clents.index') }}">--}}
{{--        <i class="nav-icon fa fa-remove"></i>--}}
{{--        <span> {{__('admin.Cancel_services')}}</span>--}}
{{--    </a>--}}
{{--</li>--}}

{{--<li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">--}}
{{--    <a class="nav-link" href="{{ route('admin.not_paid.index') }}">--}}
{{--        <i class="nav-icon fa fa-money"></i>--}}
{{--        <span> {{__('admin.not_paid')}}</span>--}}
{{--    </a>--}}
{{--</li>--}}

@if(in_array(10,\Illuminate\Support\Facades\Auth::user()->user_permissions))
    <li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.service.index') }}">
            <i class="nav-icon fa fa-gear"></i>
            <span> {{__('admin.Services')}}</span>
        </a>
    </li>

    <li class="nav-item {{ Request::is('admin.service*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.add_user.index') }}">
            <i class="nav-icon fa fa-plus"></i>
            <span> {{__('admin.Add_user')}}</span>
        </a>
    </li>
@endif
<li class="nav-item {{ Request::is('admin.about') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.about') }}">
        <i class="nav-icon fa fa-star"></i>
        <span> {{__('admin.about')}}</span>
    </a>
</li>
