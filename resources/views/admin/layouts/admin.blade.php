<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Baxtli oila</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/coreui.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/coreui-icons-free.css')}}">
    <link rel="shortcut icon" href="{{asset('/images/logo/logo2.png')}}" type="image/x-icon">
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/simple-line-icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/select2/css/select2.css')}}">


    @yield('css')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{asset('/images/logo/logo2.png')}}" height="50">
        <img class="navbar-brand-minimized" src="{{asset('/images/logo/logo2.png')}}" width="30" height="30">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link " style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->user_full_name }}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.profile.index') }}">
                    <i class="fa fa-user"></i> {{__('admin.Change_password')}}</a>
                <a href="{{ url('/logout') }}" class="dropdown-item btn btn-default btn-flat">
                    <i class="fa fa-lock"></i>Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    @include('admin.layouts.sidebar')
    <main class="main">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        <span>&copy; 2021 Softix.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://softix.uz">SOFTIX</a>
        <span>Telefon: </span>
        <a href="tel:+998975270606">+99897-(97)-527-06-06</a>

    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/coreui.min.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets/js/numeral.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2@10.js') }}"></script>
<script src="{{ asset('assets/select2/js/select2.full.js') }}"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "1500",
        "hideDuration": "100",
        "timeOut": "5000",
        "extendedTimeOut": "100",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    function sweetconfirm(url,message = '{{__('admin.Are you sure')}}') {
        Swal.fire({
            title: message,
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonText:'{{__('admin.Yes')}}',
            denyButtonText:'{{__('admin.No')}}',
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                window.location = url;
            }
        })
    }
</script>
@yield('scripts')
</html>
