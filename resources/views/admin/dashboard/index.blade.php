@extends('admin.layouts.admin')

@section('content')

<style>
    th {
        text-align: end;
    }
    h5 {
        font-family: Calibri;
        color: darkblue;
        size: 8px;
    }
    /* {{date('Y-m-d H:i:s')}} */
</style>

<div class="container-fluid m-3 pl-0 ">
    <div class="animated fadeIn">
        <div class="card card-accent-primary">
            <div class="card-header"><h3 style="color: #0b2e13; size: 26px; font-family: 'Times New Roman';">
                    <i class="fa fa-pie-chart"></i> {{__('admin.Statistika')}}</h3></div>
            <div class="card-body">
               <div class="row">
                   <div class="col-md-4">
                       <div class="card text-center" >
                           <div class="card-header">
                               <h5 ><i style="color: brown;" class="fa fa-users"></i> {{__('admin.Total_clents_number')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($accounts,0,'.',',')}} ta</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                                   <a href="{{url('admin/client/')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><i style="color: yellowgreen;" class="fa fa-gears"></i> {{__('admin.Total_of_services_type')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($services,0,'.',',')}} ta</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="{{url('admin/service/')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><b><i style="color: #6f42c1;" class="fa fa-calendar-o"></i></b> {{__('admin.Day_money')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($den,0,'.',',')}} sum</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="{{url('/admin/report/by-day')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><b><i style="color: #6f42c1;" class="fa fa-calendar"></i></b> {{__('admin.Weakj_money')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($weak,0,'.',',')}} sum</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="#" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><b><i style="color: #686868;" class="fa fa-calendar"></i></b> {{__('admin.Month_money')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($month,0,'.',',')}} sum</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="#" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><i style="color: green;" class="fa fa-check"></i>
                                    {{__('admin.Paid_services_number')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($paid_orders,0,'.',',')}} ta</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="{{url('admin/queue/')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><i style="color: red;" class="fa fa-circle"></i>
                                    {{__('admin.not_pay_services_number')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($not_paid_orders,0,'.',',')}} ta</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="{{url('admin/queue/')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card text-center">
                           <div class="card-header">
                               <h5 ><i style="color: red;" class="fa fa-remove"></i> {{__('admin.Cancel_services_number')}}</h5>
                           </div>
                           <div class="card-body">
                               <strong>{{number_format($canceled_orders,0,'.',',')}} ta</strong>
                           </div>
                           <div style="background-color: #c4c9d0;" class="card-footer text-muted">
                               <a href="{{url('admin/cancel_clents')}}" class="btn btn-primary">{{__('admin.More')}}</a>
                           </div>
                       </div>
                   </div>



               </div>
            </div>
        </div>
    </div>
</div>
@endsection
