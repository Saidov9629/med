

@extends('admin.layouts.admin')

@section('css')
    @include('admin.layouts.datatables_css')
@endsection

@section('content')
    <div class="container-fluid m-3 pl-0 ">
        <div class="animated fadeIn">
            <div class="card card-accent-primary">
                <div class="card-header">{{__('admin.Services')}}</div>
                <div class="card-body">
                    <div class="pull-left">
                        <button class="btn btn-outline-primary" id="addBtn"><span class="fa fa-plus"></span> {{__('admin.Add_services')}}</button>
                    </div>
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Kategoriya</th>
                            <th>{{__('admin.Name')}}</th>
                            <th>{{__('admin.Price')}}</th>
                            <th> {{__('admin.Edit')}}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="serviceModal" tabindex="-1" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_title"></h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body" style="background-color: #e0e0e0; ">
                    <form action="#" id="service_form">
                        @csrf
                        <input type="hidden" name="id" id="id" value="0">
                        <div class="form-group">
                            <label for="company">Xizmat turi</label>
                            <select name="category_id" id="category_id" class="form-control">
                                @foreach($categories as $item)
                                <option value="{{$item->id}}">{{$item->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="company">{{__('admin.Servese_name')}}</label>
                            <input class="form-control" id="name" name="name" type="text" >
                        </div>
                        <div class="form-group">
                            <label for="company">{{__('admin.Price')}}</label>
                            <input class="form-control" id="price" name="price" type="number" >
                        </div>

{{--                        <div class="form-group">--}}
{{--                            <label for="company">{{__('admin.Skidka_day')}}</label>--}}
{{--                            <input class="form-control" id="day" name="day" type="number" >--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <label for="company">{{__('admin.Skidka_price')}}</label>--}}
{{--                            <input class="form-control" id="second_price" name="second_price" type="number" >--}}
{{--                        </div>--}}
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('admin.Close')}}</button>
                    <button class="btn btn-success" type="button" id="save_btn">{{__('admin.Save')}}</button>
                </div>
            </div>

        </div>

    </div>
@endsection

@section('scripts')
    @include('admin.layouts.datatables_js')
    <script type="text/javascript">

        var table;
        $(document).ready(function() {

            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 1, 'asc' ]],
                ajax: {
                    url: "{{ route('admin.service.datatable') }}",
                    type: "POST",
                    data:{
                        "_token":"{{csrf_token()}}"
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',orderable: false, searchable: false},
                    {data: 'service_category.category_name'},
                    {data: 'name'},
                    {data: 'price'},
                    {
                        data: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });



            $('#addBtn').click(function () {
                $('#modal_title').html("{{__('admin.Add_services')}}");
                $('#id').val(0);
                $('#name').val('');
                $('#price').val('');
                $('#day').val('');
                $('#second_price').val('');
                $('#serviceModal').modal('show');
                $('#name').removeClass('is-invalid')
                $('#price').removeClass('is-invalid')
                $('#day').removeClass('is-invalid')
                $('#second_price').removeClass('is-invalid')
            })
            $('#save_btn').click(function () {
                $('#save_btn').text('Saving...'); //change button text
                $('#save_btn').attr('disabled',true); //set button enable

                var error = 0;
                if ($('#name').val() === ''){
                    toastr["warning"]('{{__('admin.Insert_service_name')}}','');
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false); //set button enable
                    error = 1;
                    $('#name').addClass('is-invalid')
                }else{
                    $('#name').removeClass('is-invalid')
                }

                if ($('#price').val() === '' ){
                    toastr["warning"]('{{__('admin.Insert_service_price')}}','');
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false); //set button enable
                    error = 1;
                    $('#price').addClass('is-invalid')
                }else{
                    $('#price').removeClass('is-invalid')
                }

                if ($('#day').val() === ''){
                    toastr["warning"]('{{__('admin.Insert_service_day')}}','');
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false); //set button enable
                    error = 1;
                    $('#day').addClass('is-invalid')
                }else{
                    $('#day').removeClass('is-invalid')
                }

                if ($('#second_price').val() === ''){
                    toastr["warning"]('{{__('admin.Insert_service_second_price')}}','');
                    $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                    $('#save_btn').attr('disabled',false); //set button enable
                    error = 1;
                    $('#second_price').addClass('is-invalid')
                }else{
                    $('#second_price').removeClass('is-invalid')
                }


                if (error === 0) {
                    $.ajax({
                        url : "{{route('admin.service.save')}}",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: $('#service_form').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {

                            if(data.success)
                            {
                                toastr["success"](data.message,'');
                                $('#serviceModal').modal('hide');
                                table.ajax.reload();
                            }
                            else
                            {
                                Swal.fire({
                                    // position: 'top-end',
                                    icon: 'warning',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                            table.ajax.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            Swal.fire({
                                // position: 'top-end',
                                icon: 'error',
                                title: "{{__('admin.Error server')}}",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#save_btn').text('{{__('admin.Save')}}'); //change button text
                            $('#save_btn').attr('disabled',false); //set button enable
                            table.ajax.reload();
                        }
                    });
                }


            });
        } );


        function edit(id,name,price,day,second_price,category_id) {
            $('#modal_title').html("{{__('admin.Edit_services')}}");
            $('#id').val(id);
            $('#name').val(name);
            $('#price').val(price);
            $('#day').val(day);
            $('#second_price').val(second_price);
            $('#category_id').val(category_id);
            $('#serviceModal').modal('show');
            $('#name').removeClass('is-invalid')
            $('#price').removeClass('is-invalid')
            $('#day').removeClass('is-invalid')
            $('#second_price').removeClass('is-invalid')
        }
    </script>
@endsection

