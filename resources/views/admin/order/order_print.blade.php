<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        table td,
        table th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
    </style>
    <title>{{__('admin.Warehouse_receipt')}}</title>
</head>
<body>
<div>
    <div style="display:inline-block;width:50%; font-size: 18px;">
        <center>
            <img src="{{asset('/images/logo/for_invoice.png')}}" width="100"><br>
            <h3>"BAXTLI OILA" <br> xususiy klinikasi</h3>
        </center>
    </div>
    <div style="display:inline-block;width:49%; font-size: 18px;">
        <b>Sana:</b> {{$order->created_at}}<br>
        <b>Mijoz:</b> {{$order->order_account->account_full_name}}<br>
    </div>
</div>
    <center><h2>{{$order->order_service->name}}</h2></center>
    {!! $order->diagnosis !!}
<script>
    window.onload=function () {
        window.print();
    }
</script>
</body>
</html>
