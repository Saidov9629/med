<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\Not_paidController;
use App\Http\Controllers\Admin\InqueueController;
use App\Http\Controllers\Admin\Cancel_clentsController;
use App\Http\Controllers\Admin\AdduserController;
use App\Http\Controllers\Admin\PillController;
use App\Http\Controllers\Admin\ReportController;




Route::redirect('/','/login');
Route::get('/login',    [AuthController::class,'login'])        ->name('login');
Route::post('/login',   [AuthController::class,'check_login'])  ->name('checklogin');
Route::get('/logout',   [AuthController::class,'logout'])        ->name('logout');


Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>['auth']], function() {

    Route::get('/',                                     [DashboardController::class,'index'])->name('dashboard');
    Route::view('/welcome','welcome');
    Route::view('/about','about')->name('about');

    Route::get('/client',                               [ClientController::class, 'index'])->name('clients.index');
    Route::post('/client/datatable',                    [ClientController::class, 'getDataTable'])->name('clients.datatable');
    Route::post('/client/save',                         [ClientController::class,'save'])->name('client.save');
    Route::get('/client/findDistrictName',              [ClientController::class,'findDistrictName'])  ->name('client.findDistrictName');
    Route::get('/client/view/{id}',                     [ClientController::class, 'view'])->name('clients.view');
    Route::get('/client/view-services/{id}',            [ClientController::class, 'view_with_services'])->name('clients.view_with_services');

    Route::get('/service',                              [ServiceController::class, 'index'])->name('service.index');
    Route::post('/service/datatable',                   [ServiceController::class, 'getDataTable'])->name('service.datatable');
    Route::post('/service/save',                        [ServiceController::class,'save'])->name('service.save');

    Route::post('/client/datatable',                    [ClientController::class, 'getDataTable'])->name('clients.datatable');

    Route::get('/client/make-order/{id}',               [OrderController::class, 'make_order'])->name('clients.make-order');
    Route::post('/client/make-order/{id}',              [OrderController::class, 'creat_order']);
    Route::post('/client/save-transaction/{id}',        [ClientController::class, 'save_transaction']);
    Route::get('/client/change_status/{id}/{endprocess}',[OrderController::class, 'change_status']);

    Route::post('/order/cancel/{account_id}',           [OrderController::class, 'cancel_order'])->name('order.cancel_order');
    Route::post('/order/datatable',                     [OrderController::class, 'getDataTable'])->name('order.datatable');
    Route::get('/order/print/{order_id}',                     [OrderController::class, 'print_result'])->name('order.print_result');

    Route::get('/queue/',                               [OrderController::class, 'index'])->name('queue.index');
    Route::post('/queue/datatable',                     [OrderController::class, 'getDataTable'])->name('queue.datatable');
    Route::post('/queue/save/{id}',                     [OrderController::class, 'save'])->name('queue.save');
    Route::get('/queue/diagnosis/{id}',                 [OrderController::class, 'make_diagnosis'])->name('queue.diagnosis');
    Route::POST('/queue/diagnosis/{id}',                [OrderController::class, 'creat_diagnosis']);

    Route::get('/not-paid',                             [Not_paidController::class, 'index'])->name('not_paid.index');
    Route::post('/not-paid/datatable',                  [Not_paidController::class, 'getDataTable'])->name('not_paid.datatable');
    Route::post('/not-paid/save',                       [Not_paidController::class, 'save'])->name('not_paid.save');
    Route::get('/not-paid/pay/{id}',                    [Not_paidController::class, 'make_pay'])->name('not_paid.pay');
    Route::get('/not-paid/check/{id}',                  [Not_paidController::class, 'check'])->name('not_paid.check');

    Route::get('/inprocess/',                           [InqueueController::class, 'index'])->name('inprocess.index');
    Route::post('/inprocess/datatable',                 [InqueueController::class, 'getDataTable'])->name('inprocess.datatable');

    Route::get('/cancel_clents/',                       [Cancel_clentsController::class, 'index'])->name('cancel_clents.index');
    Route::post('/cancel_clents/datatable',             [Cancel_clentsController::class, 'getDataTable'])->name('cancel_clents.datatable');

    Route::get('/profile',                              [AuthController::class,'update_pas'])   ->name('profile.index');
    Route::post('/profile',                             [AuthController::class,'check_update_pas']) ->name('checkupdatepas');

    Route::get('/add_user/',                            [AdduserController::class, 'index'])->name('add_user.index');
    Route::post('/add_user/datatable',                  [AdduserController::class, 'getDataTable'])->name('add_user.datatable');
    Route::post('/add_user/save',                       [AdduserController::class, 'save'])->name('add_user.save');

    Route::get('/pill/',                                [PillController::class, 'index'])->name('pill.index');
    Route::post('/pill/datatable',                      [PillController::class, 'getDataTable'])->name('pill.datatable');
    Route::post('/pill/save',                           [PillController::class, 'save'])->name('pill.save');
    Route::post('/pill/cargo/save',                     [PillController::class, 'cargo_save'])->name('pill.cargo_save');
    Route::get('/client/pills/{id}',                    [PillController::class, 'client_pills'])->name('pill.client_pills');
    Route::post('/client/pills/client-pill-save',       [PillController::class, 'client_pill_save'])->name('pill.client_pill_save');
    Route::post('/client/pills/datatable/{id}',         [PillController::class, 'getDataTableClientPills'])->name('pill.clientpills');
    Route::get('/client/pills/delete/{id}',             [PillController::class, 'client_pill_delete'])->name('pill.client_pill_delete');

    Route::get('/report/by-day',                         [ReportController::class, 'payments_by_date'])->name('report.payments_by_date');
});
