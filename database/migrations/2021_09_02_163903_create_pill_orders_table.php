<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePillOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pill_orders', function (Blueprint $table) {
            $table->id('pill_order_id');
            $table->bigInteger('account_id')->nullable();
            $table->bigInteger('pill_id');
            $table->bigInteger('user_id');
            $table->integer('quantity');
            $table->integer('pill_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pill_orders');
    }
}
