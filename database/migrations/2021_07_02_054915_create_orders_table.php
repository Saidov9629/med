<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments();
            $table->Integer('user_id');
            $table->Integer('account_id');
            $table->Integer('service_id');
            $table->Integer('price');
            $table->enum('status',['inprocess','endprocess','cancel']);
            $table->Integer('transaction_id')->nullable();
            $table->text('diagnosis')->nullable();
            $table->Integer('result_template_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
