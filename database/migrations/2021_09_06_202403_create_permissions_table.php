<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        $data = [
            ['id'=>1,   'name'=> "Dashboard"],
            ['id'=>2,   'name'=> "Klientlar"],
            ['id'=>3,   'name'=> "Klient qo'shish o'rzgartirish"],
            ['id'=>4,   'name'=> "Klient qarzi va xizmatlari"],
            ['id'=>5,   'name'=> "Klient navbatga qo'shish"],
            ['id'=>6,   'name'=> "Klient dori qo'shish"],
            ['id'=>7,   'name'=> "Dorilar bo'limi to'liq"],
            ['id'=>8,   'name'=> "Dorilar yuk kiritish"],
            ['id'=>9,   'name'=> "Xizmatga tashxis yozish"],
            ['id'=>10,  'name'=> "Sozlamalar"],
        ];
        \Illuminate\Support\Facades\DB::table('permissions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
