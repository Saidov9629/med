<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->id();
            $table->Integer('CODE');
            $table->string('NAME');
            $table->timestamps();
        });
        \Illuminate\Support\Facades\DB::statement("INSERT INTO `regions` (`id`, `CODE`, `NAME`, `created_at`, `updated_at`) VALUES
(1, 3, 'АНДИЖАНСКАЯ', NULL, NULL),
(2, 6, 'БУХАРСКАЯ', NULL, NULL),
(3, 8, 'ДЖИЗАКСКАЯ', NULL, NULL),
(4, 10, 'КАШКАДАРЬИНСКАЯ', NULL, NULL),
(5, 12, 'НАВОИЙСКАЯ', NULL, NULL),
(6, 14, 'НАМАНГАНСКАЯ', NULL, NULL),
(7, 18, 'САМАРКАНДСКАЯ', NULL, NULL),
(8, 22, 'СУРХАНДАРЬИНСКАЯ', NULL, NULL),
(9, 24, 'СЫРДАРЬИНСКАЯ', NULL, NULL),
(10, 26, 'г.ТАШКЕНТ', NULL, NULL),
(11, 27, 'ТАШКЕНТСКАЯ', NULL, NULL),
(12, 30, 'ФЕРГАНСКАЯ', NULL, NULL),
(13, 33, 'ХОРЕЗМСКАЯ', NULL, NULL),
(14, 35, 'РЕСП.КАРАКАЛПАКСТАН', NULL, NULL);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
