<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_full_name');
            $table->string('user_login')->unique();
            $table->string('user_password');
            $table->unsignedBigInteger('user_role_id');
            $table->enum('user_lang',['uz','ru','en'])->default('uz');
            $table->jsonb('user_data')->nullable();
            $table->jsonb('user_permissions')->nullable();
            $table->timestamps();
        });

        \App\Models\User::query()
            ->create([
                "user_full_name" => "Administrator",
                "user_login" => "admin",
                "user_password" => bcrypt("admin"),
                "user_role_id" => 1
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
