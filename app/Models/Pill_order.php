<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pill_order extends Model
{
    protected $table        = 'pill_orders';
    protected $primaryKey   = 'pill_order_id';
    protected $fillable     = [
        'account_id',
        'pill_id',
        'user_id',
        'quantity',
        'pill_price',
    ];
    protected $casts = [
        'created_at'=>'datetime:Y-m-d H:i'
    ];

    public function order_pill()
    {
        return $this->belongsTo(Pill::class, 'pill_id', 'pill_id');
    }
    public function order_account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }
    public function pill_order_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
