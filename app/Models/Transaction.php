<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';

    protected $fillable = [
        'user_id',
        'account_id',
        'payment_type',
        'credit',
        'debit'
    ];
    protected $casts = [
        'created_at'=>'datetime:Y-m-d H:i'
    ];

    public function transaction_account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }
}
