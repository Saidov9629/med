<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';
    protected $primaryKey = 'account_id';
    protected $fillable = [
        'account_type',
        'account_full_name',
        'account_phone',
        'account_adress_name',
        'account_region_code',
        'account_district_code'
    ];
    protected $types = [
        'client',
    ];

    public function account_region()
    {
        return $this->belongsTo(Region::class, 'account_region_code', 'CODE');
    }

}
