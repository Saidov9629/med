<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pill extends Model
{
    protected $table        = 'pills';
    protected $primaryKey   = 'pill_id';
    protected $fillable     = [
        'pill_name',
        'pill_price',
        'pill_quantity'
    ];
}
