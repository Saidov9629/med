<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'user_id',
        'account_id',
        'service_id',
        'price',
        'status',
        'transaction_id',
        'diagnosis',
        'result_template_id',
    ];
    public function order_account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }

    public function order_service()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }


}
