<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $table = 'service';

    public function service_category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
