<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function payments_by_date(Request $request)
    {
        if ($request->has('day')){
            $day = $request->day;
        }else{
            $day = date('Y-m-d');
        }
        $data = Transaction::selectRaw("account_id, sum(debit) as total_payed,sum( CASE WHEN payment_type = 'cash' THEN debit ELSE 0 END ) total_cash,sum( CASE WHEN payment_type = 'card' THEN debit ELSE 0 END ) total_card")
            ->where('created_at','>=',$day.' 00:00:00')
            ->where('created_at','<=',$day.' 23:59:59')
            ->whereIn('payment_type',['card','cash'])
            ->groupBy('account_id')
            ->orderBy('total_payed')
            ->get();
        $total_data = DB::select("SELECT sum(debit) as total_payed,sum( CASE WHEN payment_type = 'cash' THEN debit ELSE 0 END ) total_cash,sum( CASE WHEN payment_type = 'card' THEN debit ELSE 0 END ) total_card FROM `transaction` where created_at>='".$day." 00:00:00' and created_at<='".$day." 23:59:59' and payment_type in ('card','cash')")[0];
        return view('admin.report.payments_by_day',compact('data','day','total_data'));
    }
}
