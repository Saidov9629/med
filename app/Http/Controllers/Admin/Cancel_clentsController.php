<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class Cancel_clentsController extends Controller
{
    public function index()
    {
        return view('admin.cancel_clents.index');
    }

    public function getDataTable(Request $request)
    {
        $model = Order::query()->where('status','=','cancel')->with(['order_account','order_service']);
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->editColumn('status',function ($item){
                if ($item->status=='cancel'){
                    return "<span class='btn btn-dark'>".__('admin.Cancel_service')."</span>";
                }
            })
            ->addColumn('action',function ($item){
                return "<a href='".url('/admin/queue/diagnosis/'.$item->id)."' class='btn btn-primary'>".__('admin.Write_diagnosis')."</a>";})
            ->rawColumns(['action','status'])
            ->make(true);
    }
}
