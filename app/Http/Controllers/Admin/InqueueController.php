<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class InqueueController extends Controller
{
    public function index()
    {

        return view('admin.inprocess.index');
    }

    public function getDataTable(Request $request)
    {
        $model = Order::query()->where('status','=','inprocess')->with(['order_account','order_service']);
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->editColumn('status',function ($item){
                if ($item->status=='inprocess'){
                    return "<span class='btn btn-success'>".__('admin.inprocess')."</span>";
                }
            })
            ->addColumn('action',function ($item){
                if (in_array(9,Auth::user()->user_permissions)) {
                    return "<a href='" . url('/admin/queue/diagnosis/' . $item->id) . "' class='btn btn-primary'>" . __('admin.Write_diagnosis') . "</a>";
                }else{
                    return  '';
                }
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }
}
