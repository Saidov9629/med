<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\District;
use App\Models\Order;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class AdduserController extends Controller
{
    public function index()
    {
        $user = User::find(Auth::user()->user_id);
        return view('admin.add_user.index',compact('user'));
    }

    public function getDataTable(Request $request)
    {
        $model = User::query();
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->addColumn('action',function ($item){
                return "<button class='btn btn-success' onclick=\"edit(".$item->user_id.",'".addslashes($item->user_full_name)."','".addslashes($item->user_login)."','".addslashes($item->user_role_id)."')\"><span class='fa fa-pencil'></span></button>";            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function save(Request $request)
    {
        if (!$request->has('user_full_name') || $request->user_full_name == "" ){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Enter_user_full_name')
            ]);
        }
        if (!$request->has('user_login' ) || $request->user_login == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Enter_login')
            ]);
        }
        if (!$request->has('user_password') || $request->user_password == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.User_password')
            ]);
        }
        if (!$request->has('user_conforim_password') || $request->user_conforim_password == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Conforim_new_password')
            ]);
        }
        if ($request->user_conforim_password != $request->user_password){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Not_match_password')
            ]);
        }


        if ($request->user_id == 0 ){
            $data = new User();
            $data->user_full_name = $request->user_full_name;
            $data->user_login = $request->user_login;
            $data->user_password = bcrypt($request->user_password);
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.New_user_added'),
                "user_id"=>$data->user_id
            ]);
        }
    }
}
