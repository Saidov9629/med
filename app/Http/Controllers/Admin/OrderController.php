<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Category;
use App\Models\District;
use App\Models\Order;
use App\Models\Region;
use App\Models\Result_template;
use App\Models\Service;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class OrderController extends Controller
{
    public function index()
    {
        return view('admin.queue.index');
    }

    public function make_order($account_id){
        if (!in_array(5,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}
        $account = Account::find($account_id);
        $region = Region::query()->where('code','=',$account->account_region_code)->first();
        $district = District::query()->where('NS10_CODE','=',$account->account_region_code)->where('code','=',$account->account_district_code)->first();
        $categories = Category::query()
            ->with('category_services')
            ->get();
//        dd($categories);
        return view('admin.clients.make_order',compact('categories','account','region','district'));
    }

    public function creat_order(Request $request, $account_id){
        if (!in_array(5,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}

        $total_credit = 0;
        foreach ($request->service as $item) {
            $service = Service::find($item);
            $order              = new Order();
            $order->user_id     = Auth::user()->user_id;
            $order->account_id  = $account_id;
            $order->service_id  = $service->id;
            $order->price       = $service->price;
            $order->status      = 'inprocess';
            $order->save();
            $total_credit+=$service->price;
        }
        $transaction                = new Transaction();
        $transaction->user_id       = Auth::user()->user_id;
        $transaction->account_id    = $account_id;
        $transaction->account_id    = $account_id;
        $transaction->payment_type  = 'total';
        $transaction->credit        = $total_credit;
        $transaction->debit         = 0;
        $transaction->save();

        return redirect('admin/client');

    }

    public function change_status(Request $request, $order_id, $status){
        $order              = Order::find($order_id);
        $order->status      = $status;
        $order->save();
        return redirect('admin/client');
    }


    public function getDataTable(Request $request)
    {
        $model = Order::query()->where('status','=','inprocess')->with(['order_account','order_service']);
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->editColumn('status',function ($item){
                if ($item->status=='inprocess'){
                    return "<span class='btn btn-warning'>".__('admin.inqueue')."</span>";
                }
            })
            ->addColumn('action',function ($item){
                if (in_array(9,Auth::user()->user_permissions)) {
                    return "<a href='" . url('/admin/queue/diagnosis/' . $item->id) . "' class='btn btn-primary'>" . __('admin.Write_diagnosis') . "</a>";
                }else{
                    return '';
                }
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }



    public function make_diagnosis($order_id){
        if (!in_array(9,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}

        $order = Order::find($order_id);
        $account = Account::find($order->account_id);
        $services = Service::all();//get data from table
        $region = Region::query()->where('code','=',$account->account_region_code)->first();
        $district = District::query()->where('NS10_CODE','=',$account->account_region_code)->where('code','=',$account->account_district_code)->first();
        $result_templates = Result_template::where('service_id','=',$order->service_id)->get();
        return view('admin.queue.diagnosis',compact('services','account','region','district','order','result_templates'));
    }

    public function save(Request $request, $order_id)
    {
        if (!$request->has('diagnosis')) {
            return response()->json([
                "success" => false,
                "message" => __('admin.Please_write_diagnosis')
            ]);
        }
        if ($request->id != 0 ){
            //create
            $data                       = Order::find($order_id);
            $data->result_template_id   = $request->result_template_id;
            $data->diagnosis            = $request->diagnosis;
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.diagnosis_success_add')
            ]);
        }
    }

    public function creat_diagnosis(Request $request, $account_id){
        foreach ($request->service as $item) {
            $service = Service::find($item);
            $order              = new Order();
            $order->user_id     = Auth::user()->user_id;
            $order->account_id  = $account_id;
            $order->service_id  = $service->id;
            $order->price       = $service->price;
            $order->status      = 'not_paid';
            $order->save();
        }
        return redirect('admin/diagnosis');

    }

    public function cancel_order(Request $request, $account_id)
    {

        if ($request->summa<0 || $request->summa == ''){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Wrong data')
            ]);
        }
        $order = Order::find($request->order_id);
        if (!in_array($order->status,['inprocess', 'endprocess'])){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Order already cancelled')
            ]);
        }
        $transaction                = new Transaction();
        $transaction->user_id       = Auth::user()->user_id;
        $transaction->account_id    = $account_id;
        $transaction->payment_type  = 'cancel';
        $transaction->credit        = -1*$order->price;
        $transaction->debit         = -1*$request->summa;
        $transaction->save();
        $order->status = 'cancel';
        $order->save();
        return response()->json([
            "success"=>true,
            "message"=>__('admin.Data save successfully')
        ]);
    }

    public function print_result($order_id)
    {
        $order = Order::query()->with('order_service')->where('id','=',$order_id)->first();
        return view('admin.order.order_print',compact('order'));
    }
}
