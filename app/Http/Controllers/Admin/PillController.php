<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\District;
use App\Models\Order;
use App\Models\Pill;
use App\Models\Pill_order;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class PillController extends Controller
{
    public function index()
    {
        return view('admin.pill.index');
    }

    public function getDataTable(Request $request)
    {
        $model = Pill::query()->orderBy('created_at','desc');
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->addColumn('action',function ($item){
                $result = '';
                if (in_array(8,Auth::user()->user_permissions)) {
                    $result .= " <button type='button' onclick=\"add_cargo(".$item->pill_id.",'".addslashes($item->pill_name)."')\" class='btn btn-primary'><span class='fa fa-plus'></span> ".__('admin.Add cargo')."</button>";
                }
                if(in_array(7,Auth::user()->user_permissions))
                {
                    $result .= " <button type='button' onclick=\"edit(".$item->pill_id.",'".addslashes($item->pill_name)."',".$item->pill_price.")\" class='btn btn-primary'><span class='fa fa-edit'></span> ".__('admin.Edit')."</button>";
                }
                return $result;
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }

    public function save(Request $request)
    {
        if (!$request->has('pill_name') || $request->pill_name == "" ){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_pill_name')
            ]);
        }
        if (!$request->has('pill_price' ) || $request->pill_price == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_pill_price')
            ]);
        }



        //Create or update checking
        if ($request->pill_id == 0 ){
            $d = Pill::where('pill_name','=',$request->pill_name)->first();
            if ($d != null){
                return response()->json([
                    "success"=>false,
                    "message"=>"Bunday dori oldin kiritilgan"
                ]);
            }
            //create
            $data = new Pill();
            $data->pill_name = $request->pill_name;
            $data->pill_price = $request->pill_price;
            $data->save();
        }else{
            //update
            $data = Pill::query()
                ->where('pill_id',$request->pill_id)
                ->first();
            $data->pill_name = $request->pill_name;
            $data->pill_price = $request->pill_price;
            $data->save();
        }
        return response()->json([
            "success"=>true,
            "message"=>__('admin.Data save successfully'),
        ]);

    }

    public function client_pills($account_id)
    {
        if (!in_array(6,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}
        $account = Account::find($account_id);
        $region = Region::query()->where('code','=',$account->account_region_code)->first();
        $district = District::query()->where('NS10_CODE','=',$account->account_region_code)->where('code','=',$account->account_district_code)->first();
        $pills = Pill::orderBy('pill_name')->get();
        return view('admin.pill.client_pills',compact('account','region','district','pills'));
    }

    public function client_pill_save(Request $request)
    {
        if (!$request->has('pill_id') || $request->pill_id == "") {
            return response()->json([
                "success" => false,
                "message" => __('admin.Please select pill')
            ]);
        }
        if (!$request->has('quantity') || $request->quantity == "") {
            return response()->json([
                "success" => false,
                "message" => __('admin.Please insert quantity')
            ]);
        }
        $pill = Pill::find($request->pill_id);
        if ($pill->pill_quantity<$request->quantity){
            return response()->json([
                "success" => false,
                "message" => __('admin.Not enough in warehouse')
            ]);
        }

        $order              = new Pill_order();
        $order->pill_id     = $request->pill_id;
        $order->quantity    = $request->quantity;
        $order->pill_price  = $pill->pill_price;
        $order->account_id  = $request->account_id;
        $order->user_id     = Auth::user()->user_id;
        $order->save();
        $pill->pill_quantity -=$request->quantity;
        $pill->save();

        return response()->json([
            "success"=>true,
            "message"=>__('admin.Data save successfully')
        ]);
    }

    public function getDataTableClientPills(Request $request, $account_id)
    {
        $model = Pill_order::query()
            ->with(['order_pill','order_account','pill_order_user'])
            ->where('account_id','=',$account_id);
        return DataTables::eloquent($model)
            ->addColumn('action',function ($item){
                return '<button class="btn btn-danger" onclick="sweetconfirm(\''.url('/admin/client/pills/delete/'.$item->pill_id).'\')"><span class="fa fa-remove"></span> '.__('admin.Delete').'</button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function client_pill_delete($id)
    {
        $pill_order = Pill_order::where('pill_id','=',$id)->first();
        $pill = Pill::find($pill_order->pill_id);
        $pill->pill_quantity +=$pill_order->quantity;
        $pill->save();
        $pill_order->delete();

        return redirect()->back();
    }

    public function cargo_save(Request $request)
    {
        if ($request->quantity == '' || $request->quantity<=0){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Wrong data')
            ]);
        }
        $pill = Pill::find($request->pill_id);
        $order              = new Pill_order();
        $order->pill_id     = $request->pill_id;
        $order->quantity    = $request->quantity;
        $order->pill_price  = $pill->pill_price;
        $order->user_id     = Auth::user()->user_id;
        $order->save();
        $pill->pill_quantity +=$request->quantity;
        $pill->save();
        return response()->json([
            "success"=>true,
            "message"=>__('admin.Data save successfully')
        ]);
    }
}
