<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Category;
use App\Models\District;
use App\Models\Region;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class ServiceController extends Controller
{
    public function index()
    {
        if (!in_array(10,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}
        $categories = Category::orderBy('category_name')->get();
        return view('admin.service.index',compact('categories'));
    }

    public function getDataTable(Request $request)
    {
        $model = Service::query()->with('service_category');
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->addColumn('action',function ($item){
                return "<button class='btn btn-success' onclick=\"edit(".$item->id.",'".addslashes($item->name)."','".addslashes($item->price)."','".addslashes($item->day)."','".addslashes($item->second_price)."','".addslashes($item->category_id)."')\"><span class='fa fa-pencil'></span></button>";            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function save(Request $request)
    {
        if (!$request->has('name')){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_service_name')
            ]);
        }
        if (!$request->has('price')){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_service_price')
            ]);
        }

        //Create or update checking
        if ($request->id == 0 ){
            //create
            $data = new Service();
            $data->name = $request->name;
            $data->price = $request->price;
            $data->category_id = $request->category_id;
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.New_insert_service_add')
            ]);
        }else{
            //update
            $data = Service::query()
                ->where('id',$request->id)
                ->first();
            $data->name = $request->name;
            $data->price = $request->price;
//            $data->day = $request->day;
//            $data->second_price = $request->second_price;
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.Insert_service_update')
            ]);
        }
    }

    public function delete($id)
    {
        Service::where('id','=',$id)->delete();
        return redirect('/admin/service');
    }
}
