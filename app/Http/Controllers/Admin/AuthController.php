<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin.auth.login');
    }

    public function check_login(Request $request)
    {
        $request->validate([
            'login'     => 'required|string',
            'password'  => 'required|string',
        ]);
        $user = User::query()
            ->where('user_login',$request->login)
            ->first();
        if ($user!=null && Hash::check($request->password, $user->user_password)) {
            Auth::login($user);
            session(['my_locale' => $user->user_lang]);
            $link = session('redirect_link');
            if ($link==null){
                $link = "/admin";
            }
            session(['redirect_link'=>null]);
            return redirect($link);
        }
        return redirect(route('login'))->with('error', Lang::get('admin.login_error'));
    }

    public function update_pas(Request $request)
    {
        $user = User::find(Auth::user()->user_id);
        return view('admin.profile.index',compact('user'));
    }

    public function check_update_pas(Request $request)
    {
        $user = User::find(Auth::user()->user_id);
        $user->user_full_name = $request->user_full_name;
        $user->user_login = $request->user_login;
        if ($user!=null && Hash::check($request->old_password, $user->user_password)) {
            if($request->new_password == $request->confirm_password){
                $user->user_password = bcrypt($request->new_password);
            }
        }
        $user->save();
        return redirect()->back()->with('Success', Lang::get('admin.login_error'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
