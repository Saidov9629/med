<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\District;
use App\Models\Order;
use App\Models\Region;
use App\Models\Service;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class Not_paidController extends Controller
{
    public function index()
    {
        return view('admin.not_paid.index');
    }

    public function getDataTable(Request $request)
    {
        $model = Order::query()
            ->selectRaw('orders.account_id, sum(orders.price) as total')
            ->with('order_account')
            ->where('status','!=','cancel')
            ->whereNull('transaction_id')
            ->groupBy('account_id');
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->addColumn('action',function ($item){
                return "<a href='".url('/admin/not-paid/pay/'.$item->account_id)."' class='btn btn-success' ><span class='fa fa-eye'></span></a>";
            })
            ->editColumn('total',function ($item){
                return number_format($item->total,2);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function make_pay(Request $request, $account_id){
        $account  = Account::find($account_id);
        $services = Service::all();
        $region   = Region::query()->where('code','=',$account->account_region_code)->first();
        $district = District::query()
            ->where('NS10_CODE','=',$account->account_region_code)
            ->where('code','=',$account->account_district_code)
            ->first();
        $orders   = Order::query()
            ->where('account_id','=',$account_id)
            ->where('status','!=','cancel')
            ->whereNull('transaction_id')
            ->with('order_service')
            ->get();
        return view('admin.not_paid.pay',compact('services','account','region','district','orders'));
    }

    public function pay($account_id)
    {
        return view('admin.not_paid.pay');
    }

    public function save(Request $request)
    {
        if ($request->has('cash') && $request->cash != null){
            $new_transaction = new Transaction();
            $new_transaction->user_id = Auth::user()->user_id;
            $new_transaction->account_id = $request->account_id;
            $new_transaction->credit = 0;
            $new_transaction->debit = $request->cash;
            $new_transaction->payment_type = 'cash';
            $new_transaction->save();
        }
        if ($request->has('card') && $request->card != null){
            $new_transaction = new Transaction();
            $new_transaction->user_id = Auth::user()->user_id;
            $new_transaction->account_id = $request->account_id;
            $new_transaction->credit = 0;
            $new_transaction->debit = $request->card;
            $new_transaction->payment_type = 'card';
            $new_transaction->save();
        }
        Order::query()
            ->where('account_id','=',$request->account_id)
            ->where('status','!=','cancel')
            ->whereNull('transaction_id')
            ->update([
                "transaction_id" => $main_transaction->id
            ]);

        return redirect('/admin/not-paid')->with('sucess','Message success');
    }

    public function check($main_transaction_id)
    {
        $transaction = Transaction::find($main_transaction_id);
        $data =  Order::query()
            ->where('transaction_id','=',$main_transaction_id)
            ->with('order_service')
            ->with('order_account')
            ->get();
        return view('admin/not_paid/check',compact('transaction','data'));
    }
}
