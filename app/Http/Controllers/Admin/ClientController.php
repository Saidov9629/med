<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\District;
use App\Models\Order;
use App\Models\Region;
use App\Models\Service;
use App\Models\Transaction;
use App\Models\User;
use Brick\Math\Exception\DivisionByZeroException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    //
    public function index()
    {
        $regions = Region::all();//get data from table
        return view('admin.clients.index',compact('regions'));
    }

    public function findDistrictName(Request $request){
        $data = District::select('DNAME','CODE')->where('NS10_CODE',$request->CODE)->orderBy('DNAME')->get();
        return response()->json($data);
    }

    public function getDataTable(Request $request)
    {
        $model = Account::query()->where('account_type', '=', 'client');
        if ($request->show_type == 'today'){
            $model = $model->where('created_at','>=',date('Y-m-d 00:00:00'));
        }
        return DataTables::eloquent($model)
            ->addIndexColumn()
            ->addColumn('account_loan',function ($item){
                if (in_array(4,Auth::user()->user_permissions)) {
                    $loan = Transaction::query()
                        ->where('account_id', '=', $item->account_id)
                        ->sum(DB::raw('debit-credit'));
                    if ($loan < 0) {
                        return "<span class='badge badge-danger' style='font-size: 12px!important;'>" . number_format($loan * (-1)) . "</span>";
                    } elseif ($loan > 0) {
                        return "<span class='badge badge-success' style='font-size: 12px!important;'>" . number_format($loan) . "</span>";
                    }
                    return $loan;
                }else{
                    return '';
                }
            })
            ->addColumn('action',function ($item){
                $result= "";
                if (in_array(3,Auth::user()->user_permissions)) {
                    $result .= " <button class='btn btn-secondary' onclick=\"edit(" . $item->account_id . ",'" . addslashes($item->account_full_name) . "','" . addslashes($item->account_phone) . "','" . addslashes($item->account_region_code) . "','" . addslashes($item->account_district_code) . "','" . addslashes($item->account_adress_name) . "')\"><span class='fa fa-pencil'></span></button>";
                }

                if (in_array(4,Auth::user()->user_permissions)) {
                    $result .= " <a href='" . url('/admin/client/view-services/' . $item->account_id) . "' class='btn btn-primary'><span class='fa fa-eye'></span></a>";
                    $result .= " <a href='" . url('/admin/client/view/' . $item->account_id) . "' class='btn btn-warning'><span class='fa fa-money'></span></a>";
                }
                if (in_array(5,Auth::user()->user_permissions)) {
                    $result .= " <a href='" . url('/admin/client/make-order/' . $item->account_id) . "' class='btn btn-success'><span class='fa fa-plus'></span></a>";
                }
                if (in_array(6,Auth::user()->user_permissions)) {
                    $result .= " <a href='" . url('/admin/client/pills/' . $item->account_id) . "' class='btn btn-danger'><span class='fa fa-medkit'></span></a>";
                }
                return $result;
            })
            ->rawColumns(['action','account_loan'])
            ->make(true);
    }


    public function save(Request $request)
    {
        if (!$request->has('account_full_name') || $request->account_full_name == "" ){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_client_name')
            ]);
        }
        if (!$request->has('account_phone' ) || $request->account_phone == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_client_phone')
            ]);
        }
        if (!$request->has('account_region_code') || $request->account_region_code == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_client_region')
            ]);
        }
        if (!$request->has('account_district_code') || $request->account_district_code == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_client_district')
            ]);
        }
        if (!$request->has('account_adress_name') || $request->account_adress_name == ""){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Insert_client_adress')
            ]);
        }
        //Create or update checking
        if ($request->account_id == 0 ){
            //create
            $data = new Account();
            $data->account_type = 'client';
            $data->account_full_name = $request->account_full_name;
            $data->account_phone = $request->account_phone;
            $data->account_region_code = $request->account_region_code;
            $data->account_district_code = $request->account_district_code;
            $data->account_adress_name = $request->account_adress_name;
            $data->user_id = Auth::user()->user_id;
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.New_client_added'),
                "account_id"=>$data->account_id
            ]);
        }else{
            //update
            $data = Account::query()
                ->where('account_id',$request->account_id)
                ->first();
            $data->account_full_name = $request->account_full_name;
            $data->account_phone = $request->account_phone;
            $data->account_region_code = $request->account_region_code;
            $data->account_district_code = $request->account_district_code;
            $data->account_adress_name = $request->account_adress_name;
            $data->user_id = Auth::user()->user_id;
            $data->save();
            return response()->json([
                "success"=>true,
                "message"=>__('admin.Client_edited'),

            ]);
        }
    }

    public function view($account_id)
    {
        if (!in_array(4,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}
        $account = Account::where('account_id','=',$account_id)->with(['account_region'])->first();
        $account_district = District::where('NS10_CODE','=',$account->account_region_code)->where('CODE','=',$account->account_district_code)->first();
        $loan = Transaction::query()
            ->where('account_id','=',$account->account_id)
            ->sum(DB::raw('debit-credit'));
        if ($loan<0){
            $account_loan = "<span class='badge badge-danger' style='font-size: 12px!important;'>".number_format($loan*(-1))."</span>";
        }elseif ($loan>0){
            $account_loan = "<span class='badge badge-success' style='font-size: 12px!important;'>".number_format($loan)."</span>";
        }else{
            $account_loan = 0;
        }
        $transactions = Transaction::where('account_id','=',$account_id)->where('debit','!=',0)->orderByDesc('created_at')->paginate(20);
        return view('admin.clients.view',compact('account','account_district','account_loan','transactions'));
    }

    public function view_with_services($account_id)
    {
        if (!in_array(4,Auth::user()->user_permissions)) {return redirect('/admin/welcome');}
        $account = Account::where('account_id','=',$account_id)->with(['account_region'])->first();
        $account_district = District::where('NS10_CODE','=',$account->account_region_code)->where('CODE','=',$account->account_district_code)->first();
        $loan = Transaction::query()
            ->where('account_id','=',$account->account_id)
            ->sum(DB::raw('debit-credit'));
        if ($loan<0){
            $account_loan = "<span class='badge badge-danger' style='font-size: 12px!important;'>".number_format($loan*(-1))."</span>";
        }elseif ($loan>0){
            $account_loan = "<span class='badge badge-success' style='font-size: 12px!important;'>".number_format($loan)."</span>";
        }else{
            $account_loan = 0;
        }
        $services = Order::with('order_service')
            ->where('account_id','=',$account_id)
            ->orderByDesc('created_at')
            ->paginate(20);
        return view('admin.clients.view_with_services',compact('account','account_district','account_loan','services'));
    }

    public function save_transaction(Request $request, $account_id)
    {
        if ($request->summa <=0 || $request->summa == ''){
            return response()->json([
                "success"=>false,
                "message"=>__('admin.Wrong data')
            ]);
        }
        $data               = new Transaction();
        $data->user_id      = Auth::user()->user_id;
        $data->account_id   = $account_id;
        $data->payment_type = $request->payment_type;
        $data->credit       = 0;
        $data->debit        = $request->summa;
        $data->save();
        return response()->json([
            "success"=>true,
            "message"=>__('admin.Data save successfully')
        ]);
    }
}
