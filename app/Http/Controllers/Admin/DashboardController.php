<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Order;
use App\Models\Service;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if (!in_array(1,Auth::user()->user_permissions)){return redirect('/admin/welcome');}
        $accounts       = Account::count();
        $services       = Service::count();
        $paid_orders   = Order::query()->whereNotNull('transaction_id')->where('status','!=','cancel')->count();
        $not_paid_orders   = Order::query()->whereNull('transaction_id')->where('status','!=','cancel')->count();
        $canceled_orders  = Order::query()->where('status','=','cancel')->count();
        $den            = Transaction::query()->where('created_at','>',date( "Y-m-d 00:00:00"))->whereIn('payment_type',['cash','card'])->sum('debit');
        $weak           = Transaction::query()->where('created_at','>=',date( "Y-m-d 00:00:00", strtotime( "-6 days" ) ))->whereIn('payment_type',['cash','card'])->sum('debit');
        $month          = Transaction::query()->where('created_at','>=',date( "Y-m-d  00:00:00", strtotime( "-30 days" ) ))->whereIn('payment_type',['cash','card'])->sum('debit');

        return view('admin.dashboard.index',compact('accounts','services',
            'paid_orders','not_paid_orders','canceled_orders','den','weak','month'));
    }
}
